<?php 
    include 'layout/navbar.php';
   if(isset($_POST['update'])){
   
     $alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
     $addoncapacity = 0;
     $currentaddon = 0;
     foreach($_POST['roomqty'] as $room_id => $roomqty){
         if($roomqty  != 0){
            $fetchcapacity = $conn->query("SELECT * FROM room_masterfile WHERE room_id = '$room_id'");
            // $fetchcapacity = mysqli_query($conn, "SELECT * FROM room_masterfile WHERE room_id = {$room_id}");
            $rooms = mysqli_fetch_assoc($fetchcapacity);
            $addoncapacity += $rooms['addon_capacity'] * $roomqty;
         }
     }
     foreach ($_POST['addons'] as $addon_id => $addonqty){
         $currentaddon+= $addonqty;
     }
     $success = true;
     if($addoncapacity < $currentaddon){
        $_POST = array();
        echo "<script>alert('Addon capacity exceeded.');</script>";
        $success = false;
 
     }
     $code = '';
     do{
       $code ='';
       for($x = 0; $x <= 10; $x++){
         $code .= $alphanum[rand(0, strlen($alphanum)-1)];
       }#random 0-26;
     }
     while (
        $res = $conn->query("SELECT * FROM walkinreservation_masterfile WHERE code ='{$code}'");
        mysqli_num_rows($res) != 0);
        $total = 0;
        $daydiff = (strtotime($_POST['checkOutDate']) - strtotime($_POST['checkInDate']))/(60*60*24);
        $hashedpwd = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $overrall = 0;
        foreach ($_POST['roomqty'] as $room_id => $roomqty) {
            if($roomqty != 0){
                //COMPUTE
                $fetchrate = $conn->query("SELECT * FROM room_masterfile WHERE room_id = '$room_id'");
                // $fetchrate = mysqli_query($conn, "SELECT * FROM room_masterfile WHERE room_id = {$room_id}");
                $room = mysqli_fetch_assoc($fetchrate);
                $total = $room['room_rate'] * $daydiff * $roomqty;
                $overrall += $total;

                // ASSIGN IN A ROOM
                $checkOutDate = date("Y-m-d", strtotime($_POST['checkOutDate']));
                $getallwalkrinrooms = $conn->query("SELECT * FROM walkinrooms_masterfile WHERE room_id = '$room_id'");
                // $getallwalkrinrooms = mysqli_query($conn, "SELECT * FROM walkinrooms_masterfile WHERE room_id = {$room_id}") or die(mysqli_error($conn));
                $i = 0;
                while($row = mysqli_fetch_assoc($getallwalkrinrooms)){
                if($i == $roomqty)
                    break;
                $checkInDate = date("Y-m-d", strtotime($_POST['checkInDate']));
                $getreservedroom = mysqli_query($conn, "SELECT * FROM assignedroom_masterfile WHERE (date >= '{$checkInDate}' AND date <= '{$checkOutDate}') AND room_id = {$row['walkinrooms_id']} ") or die(mysqli_error($conn));
                if(mysqli_num_rows($getreservedroom) == 0){
                    while($checkInDate <= $checkOutDate){
                    mysqli_query($conn, "INSERT INTO assignedroom_masterfile(room_id,date, status, type, code)VALUES({$row['walkinrooms_id']}, '{$checkInDate}','Reserved','Walkin','{$code}')") or die(mysqli_error($conn));
                $checkInDate = date("Y-m-d", strtotime($checkInDate) + (60*60*24)); // +1 day per loop
            }
            }
            $i++;
    }
     $downpayment = $total * 0.15;
      
       mysqli_query($conn, "INSERT INTO walkinreservation_masterfile(room_id, checkindate, checkoutdate, firstname, lastname, code, balance, total, email, password,quantity, status) VALUES
         ('{$room_id}', '{$_POST['checkInDate']}', '{$_POST['checkOutDate']}', '{$_POST['fname']}', '{$_POST['lname']}', '{$code}', {$total},{$total}, '{$_POST['email']}', '{$hashedpwd}',{$roomqty}, 'Pending')") or die(mysqli_error($conn));
    foreach($_POST['addons'] as $addon_id => $addonqty){
        if($addonqty != 0){
       $fetchaddonrate = mysqli_query($conn, "SELECT * FROM addons_masterfile WHERE Addon_ID = {$addon_id}");
       $addon = mysqli_fetch_assoc($fetchaddonrate);
       $addontotal = $addon['Addon_rate'] * $addonqty;
       $overall+= $addontotal;
       $fetchnew = mysqli_query($conn, "SELECT max(reservation_id) FROM walkinreservation_masterfile");
       $get = mysqli_fetch_assoc($fetchnew);
       mysqli_query($conn, "UPDATE walkinreservation_masterfile SET balance = balance + {$addontotal}, total = total + {$addontotal} WHERE reservation_id = {$get['max(reservation_id)']}");
       mysqli_query($conn, "INSERT INTO walkinaddons_masterfile (addon_id, quantity, reservation_id) VALUES({$addon_id}, {$addonqty}, {$get['max(reservation_id)']})");
        }
   }
    $_POST['addons'] = array();
   }
   }
   if($success){
         echo "<script>alert('Success!')</script>";
   }
   mysqli_query($conn, "INSERT INTO walkinbilling_masterfile(code, balance, total) VALUES('{$code}',{$overrall}, {$overrall})");
   
   //$_POST = array();
   }
   ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
     

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Walk-in</h1>
                    </div>
                    <!-- Start Here -->
                    <!-- /.col-lg-12 -->
                    <form method ='post'>
                        <div class ='row'>
                            <div class ='col-md-4'>
                            <div class ='form-group'>
                                <label>Check In</label>
                                <input type ='text' class ='form-control' readonly name ='checkInDate' value = '<?php echo (isset($_POST['check']))? $_POST['checkInDate'] : '' ;?>' id ='checkInDate'/>
                            </div>
                            </div>
                            <div class ='col-md-4'>
                            <div class ='form-group'>
                                <label>Check out</label>
                                <input type ='text' class ='form-control' readonly name ='checkOutDate' value ='<?php echo (isset($_POST['check']))? $_POST['checkOutDate'] : '' ; ?>'id = 'checkOutDate'/>
                            </div>
                            </div>
                            <div class ='col-md-4'>
                            <div class ='form-group'>
                                <label>&nbsp;</label>
                                <input type ='submit' class ='btn btn-primary btn-block' name ='check' value ='Check'/>
                            </div>
                            </div>
                        </div>
                    </form>
                    <?php if(isset($_POST['check'])){ ?>
                        <form method ='post'>
                            <table  class ='table table-striped display dataTable' id = 'walkinrooms'>
                                <thead>
                                <tr>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $fetchrooms = mysqli_query($conn, "SELECT * FROM room_masterfile");
                                    while($row = mysqli_fetch_assoc($fetchrooms)){ 
                                        $fetchReservedrooms = mysqli_query($conn, "SELECT room_number FROM reservation_masterfile WHERE (((checkoutdate >= '{$_POST['checkInDate']}' AND checkindate <= '{$_POST['checkInDate']}') OR (checkoutdate >='{$_POST['checkOutDate']}' AND checkindate <= '{$_POST['checkOutDate']}')) AND room_id = {$row['room_id']}) AND (status = 'Approved' or status = 'Checkin')") or die(mysqli_error($conn));
                                        $fetchreservedwalkin = mysqli_query($conn, "SELECT quantity FROM walkinreservation_masterfile WHERE (((checkoutdate >= '{$_POST['checkInDate']}' AND checkindate <= '{$_POST['checkInDate']}') OR (checkoutdate >='{$_POST['checkOutDate']}' AND checkindate <= '{$_POST['checkOutDate']}')) AND room_id = {$row['room_id']})") or die(mysqli_error($conn));
                                        $reservedwalkin = 0;
                                        while($row1 = mysqli_fetch_assoc($fetchreservedwalkin)){
                                        $reservedwalkin += $row1['quantity'];
                                        }
                                        $reservedroomSum = 0;
                                        while($row1 = mysqli_fetch_assoc($fetchReservedrooms)){
                                        $reservedroomSum += $row1['room_number'];
                                        }
                                        $reservedroomSum += $reservedwalkin;?>
                                <tr>
                                    <td style ='width:500px'><img src ='../<?=$row['room_imagepath']?>' width ='100%'/></td>
                                    <td><?= $row['room_type']?></td>
                                    <td><?=$row['room_description']?></td>
                                    <td>
                                        <div class ='form-group'>
                                            <select class= 'form-control' name= 'roomqty[<?=$row['room_id']?>]'>
                                            <?php 
                                            for($i = 0; $i <= $row['room_number'] - $reservedroomSum; $i++){
                                                echo "<option value ='{$i}'>{$i}</option>";
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                            <table id = 'walkinaddons' class ='table table-striped display' style ='display:none'>
                                <thead>
                                <tr>
                                    <th colspan ='3' align ='center'>Avail Addons</th>
                                </tr>
                                <tr>
                                    <th>Addon name</th>
                                    <th>Rate</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <form method ='POST'>
                                        <?php $fetchaddons = mysqli_query($conn, "SELECT * FROM addons_masterfile");
                                        while($row = mysqli_fetch_assoc($fetchaddons)){
                                                    $fetchreservedaddons = mysqli_query($conn, "SELECT guestaddons_masterfile.quantity FROM guestaddons_masterfile JOIN reservation_masterfile ON guestaddons_masterfile.reservation_id = reservation_masterfile.reservation_id WHERE (((checkoutdate >= '{$_POST['checkInDate']}' AND checkindate <= '{$_POST['checkInDate']}') OR (checkoutdate >='{$_POST['checkOutDate']}' AND checkindate <= '{$_POST['checkOutDate']}')) AND guestaddons_masterfile.addons_id = {$row['Addon_ID']}) AND (status = 'Approved' or status = 'Checkin')") or die(mysqli_error($conn));
                                                    $reservedaddons = 0;
                                                    while($guestaddons = mysqli_fetch_assoc($fetchreservedaddons)){
                                                    $reservedaddons += $guestaddons['quantity'];
                                                    }
                                            $fetchwalkinaddons = mysqli_query($conn, "SELECT *, walkinaddons_masterfile.quantity as walkinaddonquantity FROM walkinaddons_masterfile INNER JOIN walkinreservation_masterfile ON walkinaddons_masterfile.reservation_id = walkinreservation_masterfile.reservation_id WHERE (((checkoutdate >= '{$_POST['checkInDate']}' AND checkindate <= '{$_POST['checkInDate']}') OR (checkoutdate >='{$_POST['checkOutDate']}' AND checkindate <= '{$_POST['checkOutDate']}')) AND walkinaddons_masterfile.addon_id = {$row['Addon_ID']}) AND (status != 'Checkout')") or die(mysqli_error($conn));
                                            $reservedwalkin = 0;
                                            while($walkinaddons = mysqli_fetch_assoc($fetchwalkinaddons)){
                                            $reservedwalkin += $walkinaddons['walkinaddonquantity'];
                                            }
                                            $reservedtotal = $reservedaddons + $reservedwalkin;
                                        ?>
                                        <tr>
                                        <td><?= $row['Addon_name'] ?></td>
                                        <td><?=$row['Addon_rate'] ?> PHP</td>
                                        <td>
                                        <select class ='form-control' name ='addons[<?= $row['Addon_ID'] 
                                        ?>]'>
                                        <?php 
                                        for($i=0; $i <= $row['Addon_qty'] - $reservedtotal; $i++){
                                            echo "<option value = '{$i}'>{$i}</option>";
                                        }
                                        ?>
                                        </select></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </form>
                                    </tbody>
                                </table>
                                <div align ='center'>
                                <button href='#' class ='btn btn-info btn-pull' id ='addons'>Proceed to addons</button>
                                <button href ='#' class ='btn btn-info btn-pull hiddenBtn' style ='display:none' id ='goback'>Go back</button>
                                <button href ='#' data-toggle ='modal' data-target = '#editreservation' class ='btn btn-info btn-pull hiddenBtn' style ='display:none'>Continue</button>
                            </div>
                            <div class="modal fade" id ='editreservation' tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Enter personal information to continue</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class ='form-group'>
                                            <input name ='room_id' type ='hidden' value =''/>
                                            <input name ='checkInDate' type ='hidden' value ='<?= $_POST['checkInDate'] ?>'/>
                                            <input name ='checkOutDate' type ='hidden' value ='<?= $_POST['checkOutDate'] ?>'/>
                                            <label>First name</label>
                                            <input type ='text' class = 'form-control' name ='fname' REQUIRED/>
                                        </div>
                                        <div class ='form-group'>
                                            <label>Last name</label>
                                            <input type ='text' class ='form-control' name ='lname' REQUIRED/>
                                        </div>
                                        <div class ='form-group'>
                                            <label>email</label>
                                            <input type ='email' class ='form-control' name ='email' REQUIRED/>
                                        </div>
                                        <div class ='form-group'>
                                            <label>Password</label>
                                            <input type ='password' class ='form-control' name ='password' REQUIRED/>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type ='hidden' name = 'roomId'>
                                        <button name = 'update' type = 'submit' class='btn btn-primary btn-block'>Reserve</button>
                                    </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                            }
                            else{
                            echo "<p style='text-align:center'>Input a date to see available rooms</p>";
                            }
                        ?>
                        <footer class="sticky-footer">
                            <div class="container">
                                <div class="text-center">
                                <small>Copyright © HomeTown Hotel Makati</small>
                                </div>
                            </div>
                        </footer>
                        <!-- Scroll to Top Button-->
                        <a class="scroll-to-top rounded" href="#page-top">
                            <i class="fa fa-angle-up"></i>
                        </a>
                        <!-- Logout Modal-->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                    <a class="btn btn-primary" href="login.php">Logout</a>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Edit Modal -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>



    <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <!-- Core plugin JavaScript-->
    <!-- <script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->
    <!-- Page level plugin JavaScript-->
    <!-- <script src="vendor/chart.js/Chart.min.js"></script> -->
    <!-- Custom scripts for all pages-->
    <!-- <script src="js/sb-admin.min.js"></script> -->
    <!-- Custom scripts for this page-->
    <!-- DataTable-->
    <!-- <script type = 'text/javascript' src ='js/datatables.min.js'></script>  -->
    <!-- <script type ='text/javascript' src = 'js/dataTables.bootstrap4.min.js'></script> -->
    <!-- Datepicker-->
    <!-- <script src="js/jquery.datetimepicker.full.min.js"></script> -->
    <!--datepicker-->
    <script src = "../vendor/datepicker/jquery.datetimepicker.full.min.js"></script>
    <script src = '../js/edit_reservation.js'></script>
    <script>
    $(document).ready(function(){
            $("#date").datetimepicker({
        timepicker: false,
        format: "Y-m-d",
        onShow:function(ct){
        this.setOptions({
            minDate: $('#checkInDate').val()
        })
        }
    })
        $('button.reserve').click(function(){
            alert('asdsad')
        $('input[name=room_id').val($(this).closest('tr').find('#room-id').html())
        })
            $('#addons').click(function(){
            $('#walkinrooms').hide()
            $('#walkinaddons').show()
            $(this).hide()
            $('.hiddenBtn').show()
        })
        $('#goback').click(function(){
            $('#walkinrooms').show()
            $('#walkinaddons').hide()
            $('.hiddenBtn').hide()
            $('#addons').show()
            
            
        })
    })
    </script>
</body>

</html>
