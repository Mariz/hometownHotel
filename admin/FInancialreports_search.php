<?php
include "../db.php";

$start = $_GET['start'];
$end = $_GET['end'];
$month = $_GET['month'];


$fetch_all_reservation = $conn->query("SELECT * FROM financialreports_masterfile 
JOIN billing_masterfile ON billing_masterfile.billing_id = billing_masterfile.billing_id
JOIN guest_masterfile ON guest_masterfile.guest_id = billing_masterfile.guest_id
JOIN reservation_masterfile ON reservation_masterfile.reservation_id = billing_masterfile.reservation_id
WHERE checkindate BETWEEN '$start' AND '$end' ");
   
?>
<table class ='table table-striped display dataTable table-responsive'>
<thead>
    <tr>
    <th>Financial Report ID</th>
    <th>Payment</th>
    <th>Payment Type</th>
    <th>Created at</th>
    </tr>
</thead>
<tbody>
<?php
while ($rows = mysqli_fetch_assoc($fetch_all_reservation)) {
    $financialreport_id = $rows['financialreport_id'];
    $payment= $rows['payment'];
    $type = $rows['payment_type'];
    $created_At = $rows['created_at'];
    ?>
<tr>
<td><?php echo $financialreport_id; ?></td>
<td><?php echo $payment; ?></td>
<td><?php echo $type; ?></td>
<td><?php echo $created_At; ?></td>

</tr>

<?php
}

?>

</tbody>
<tfoot></tfoot>
</table>
