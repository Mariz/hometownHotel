<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">
    <?php include 'layout/navbar.php';?>
     

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Blank</h1>
                    </div>
                    <!-- Start Here -->
                    <table id ='thisTable' class ='table table-striped display dataTable table-responsive'>

                        <thead>

                            <tr>

                            <th>Receipt ID</th>

                            <th>Guest ID</th>

                            <th>Name</th>

                            <th>Actions</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $fetchallreservation = mysqli_query($conn, "SELECT * FROM receipts_masterfile JOIN guest_masterfile ON guest_masterfile.guest_ID = receipts_masterfile.guest_id");

                            $currentTime = date("Y-m-d");

                            while($row = mysqli_fetch_assoc($fetchallreservation)){ ?>

                            <tr>

                            <td id = 'guest-id' ><?= $row['receipts_id'] ?></td>

                            <td ><?= $row['guest_id']?></td>

                            <td ><?= "{$row['guest_firstname']} {$row['guest_lastname']}"?></td>

                            <td>

                                <a href = "printreceipt.php?receipt_id=<?=$row['receipts_id']?>" class ='btn btn-success'>View</a>

                            </td>

                        </tr>

                        <?php } ?>

                        </tbody>

                        <tfoot></tfoot>

                    </table>

                    <footer class="sticky-footer">

                    <div class="container">

                        <div class="text-center">

                        <small>Copyright © HomeTown Hotel Makati</small>

                        </div>

                    </div>

                    </footer>

                    <!-- Scroll to Top Button-->

                    <a class="scroll-to-top rounded" href="#page-top">

                    <i class="fa fa-angle-up"></i>

                    </a>



                    <!-- Logout Modal-->

                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                    <div class="modal-dialog" role="document">

                        <div class="modal-content">

                        <div class="modal-header">

                            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>

                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">×</span>

                            </button>

                        </div>

                        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>

                        <div class="modal-footer">

                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                            <a class="btn btn-primary" href="login.php">Logout</a>

                        </div>

                        </div>

                    </div>

                    </div>

                    <!-- Edit Modal -->

                    <div class="modal fade" id ='editreservation' tabindex="-1" role="dialog">

                    <div class="modal-dialog" role="document">

                        <div class="modal-content">

                        <div class="modal-header">

                            <h5 class="modal-title">Edit</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span>

                            </button>

                        </div>

                        <div class="modal-body">

                            <form id="formEditproof" enctype="multipart/form-data" method ='post'>

                            <div class='container-fluid'>

                                <input type ='hidden' name ='t_id' />

                                <input type ='hidden' name ='old_img'/>

                                <div class='form-group'>

                                <input type ='file' class ='form-control' name = 'img' value = ''/>

                                </div>

                            </div>



                            </div>

                            <div class="modal-footer">

                            <input type ='hidden' name = 'roomId'>

                            <button name = 'update' type = 'submit' class='btn btn-primary btn-block'>Update Proof of payment</button>

                            </div>

                        </form>

                        </div>

                    </div>

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
