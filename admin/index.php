<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

     <!-- Bootstrap core CSS-->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin.css" rel="stylesheet">


    <!-- HomeTown Hotel Custom CSS -->
    <link href="../dist/css/hometownhotel.css" rel="stylesheet">

</head>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #EC644B;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
  background: #f5a5a8;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.form .register-form {
  display: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 300px;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
.container .info {
  margin: 50px auto;
  text-align: center;
}
.container .info h1 {
  margin: 0 0 15px;
  padding: 0;
  font-size: 36px;
  font-weight: 300;
  color: #1a1a1a;
}
.container .info span {
  color: #4d4d4d;
  font-size: 12px;
}
.container .info span a {
  color: #000000;
  text-decoration: none;
}
.container .info span .fa {
  color: #EF3B3A;
}
body {
  background: #EC644B; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #EC644B, #EC644B);
  background: -moz-linear-gradient(right, #EC644B, #EC644B);
  background: -o-linear-gradient(right, #EC644B, #EC644B);
  background: linear-gradient(to left, #EC644B, #EC644B);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
</style>
<body>

<div class="container">
    <div class="login-page">
        <div class="form">
            <!-- <form class="register-form" method = "POST">
                <!-- <input type="text" placeholder="name"/>
                <input type="password" placeholder="password"/>
                <input type="text" placeholder="email address"/>
                <button>create</button>
                <p class="message">Already registered? <a href="#">Sign In</a></p> -->
            <!-- </form> -->
            <form class="login-form" method = "POST">
                <label for="exampleInputEmail1">Email address</label>
                <input required class="form-control" name = "email" type="email" aria-describedby="emailHelp" placeholder="Enter email">
                <label for="exampleInputPassword1">Password</label>
                <input required class="form-control" name = "password" type="password" placeholder="Password">
                
                <button name = "submit">login</button>
                <!-- <p class="message">Not registered? <a href="#">Create an account</a></p> -->
            </form>
        </div>
    </div>
        <!-- <form method = "POST"> -->
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input required class="form-control" id="exampleInputEmail1" name = "email" type="email" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input required class="form-control" id="exampleInputPassword1" name = "password" type="password" placeholder="Password">
          </div> -->
          <!-- <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Remember Password</label>
            </div>
          </div> -->
          <!-- <button name = "submit" class="btn btn-primary btn-block">Login</button> -->
        <!-- </form> -->
       <!--  <div class="text-center">
          <a class="d-block small mt-3" href="register.php">Register an Account</a>
        </div> -->
      </div>
  </div>
  <?php 
  include '../config/db.php';
  if (isset($_POST['submit'])) {
    $email     = mysqli_real_escape_string($conn, $_POST['email']);
    $password  = mysqli_real_escape_string($conn, $_POST['password']);

    $query  = "SELECT * FROM adminuser_masterfile WHERE email='$email'";
    $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
    $row    = mysqli_fetch_assoc($result);
    $rows   = mysqli_num_rows($result);
    
    if ($rows > 0 && password_verify($password, $row['password'])) {
    // kapag walang email
    //if($row['password'] !== $password){
    // kapag mali password
    // header("Location: login.php?login=Incorrect+username+or+password");
      $_SESSION['adminlogin']     = true;
      $_SESSION['user_id']   = $row['user_id'];
      $_SESSION['firstName'] = $row['User_firstname'];
      $_SESSION['lastName']  = $row['User_lastname'];
      $_SESSION['email']     = $row['email'];
      $_SESSION['adminType'] = $row['admin_type'];
      echo "<script>window.location.href= 'adminPanel.php'</script>";
    } else {
      echo "<script>alert('Incorrect Username or Password');location.href='index.php';</script>";
      exit();
    }
  }?>

  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
